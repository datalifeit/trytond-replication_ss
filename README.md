datalife_replication_ss
=======================

The replication_ss module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-replication_ss/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-replication_ss)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
